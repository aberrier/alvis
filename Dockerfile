FROM python:3.6

COPY ./requirements.txt /requirements.txt
RUN apt-get install unzip
RUN pip install --no-cache-dir -r /requirements.txt
RUN wget https://chromedriver.storage.googleapis.com/2.27/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip
RUN mv chromedriver /usr/local/bin
COPY . /app
WORKDIR /app

ENTRYPOINT ["python",  "start.py"]

