from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())
import requests
import threading
import json
import os
from datetime import datetime, timedelta
from selenium import webdriver
from time import sleep
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


def send_message(message):
    actions = ActionChains(driver)
    actions.send_keys(message)
    actions.send_keys(Keys.RETURN)
    actions.perform()


user = ""
pwd = ""
options = webdriver.FirefoxOptions()
options.add_argument("--headless")
driver = webdriver.Firefox(options=options)
driver_culture = webdriver.Firefox(options=options)
print("Running...")
driver.get("https://www.messenger.com/t/{}".format(os.environ.get('CONVERSATION_ID')))
driver.implicitly_wait(2)
email_field = driver.find_element_by_css_selector('#email')
driver.implicitly_wait(1)
email_field.send_keys(os.environ.get('BOT_EMAIL'))
passwd_field = driver.find_element_by_css_selector('#pass')
driver.implicitly_wait(1)
passwd_field.send_keys(os.environ.get('BOT_PASSWD'))
passwd_field.send_keys(Keys.ENTER)
driver.implicitly_wait(1)
treated_dict = {}
now = datetime.now()
count = datetime.now() + timedelta(seconds=30)
print('Finished setup')
threading.Timer(30, send_message, ["Désolé j'ai du m'absenter, me voilà de retour !"]).start()

while (True):
    message_wrappers = driver.find_elements_by_class_name('_3058')
    driver.implicitly_wait(1)
    for msg in message_wrappers:
        message_id = msg.id
        if datetime.now() >= count:
            if message_id and treated_dict.get(message_id) is None:

                content = str(msg.get_attribute('body'))
                print(content)
                if content.startswith('@surirobot '):
                    text = content.split('@surirobot ')[1]
                    user_id = msg.get_attribute('threadid')
                    if user_id is None:
                        user_id = 'DEFAULT'
                    print(text)
                    data = {"text": text, "language": 'fr-FR', "user_id": user_id}
                    r = requests.post(url=os.environ.get('CONVERSE_URL'), data=json.dumps(data),
                                      headers={'Content-Type': 'application/json'})
                    if r.status_code == 200:
                        send_message(r.json()['message'])
                    else:
                        print(r.content)
                if content.startswith('@culture'):
                    driver_culture.get('https://secouchermoinsbete.fr/random')
                    driver_culture.implicitly_wait(2)
                    anecdote = driver_culture.find_element_by_css_selector('p.summary a')
                    send_message(anecdote.text)
        else:
            print('REBOOT STILL IN PROGRESS')
        treated_dict[message_id] = True
    sleep(0.2)
# driver.close()
